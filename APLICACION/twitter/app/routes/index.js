'use strict'

const express = require('express');

/*************Instancias controladores administrador****************/
const user = require('../controllers/twitts/posts');
/*******************************************************************/

const api = express.Router();

api.get("/", user.inicio);


module.exports = api;