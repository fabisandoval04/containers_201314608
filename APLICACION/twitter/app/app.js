'use strict'
const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');

const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server,{ origins: '*:*'});

//io.set('origins', 'http://localhost:8000'); 

const api = require('./routes');

let fp = require('path');


//Se declara path para el directorio public para los assets.
function relative(path) {
    return fp.join(__dirname, path);
  }
app.use(express.static(relative('public')));

//Se usará JSON para la extracción de información
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(express.static('public'));
app.use(session({
  secret: 'FabiSopes1',
  resave: false,
  saveUninitialized: true
}));

//Se declara la extensión de los archivos de handlebars y una página o páginas base como Master Pages.
/**app.engine('.hbs',hbs({
    defaultLayout: 'default',
    extname: '.hbs'
}));**/

//app.set('view engine','.hbs');

app.set('views', fp.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//Se inyectan las rutas que se utilizaran con las funciones asociadas a cada ruta con su renderizado.
app.use(api);


const mongoose = require('mongoose');
const mongo = require('mongodb').MongoClient;
const url = 'mongodb://root:root@34.68.79.159/:27017'


var usuario = mongoose.model('users',{ alias : String, nombre : String})
var twitt = mongoose.model('posts',{ alias : String, nombre : String, txt : String, categoria : String })


function guardarTwitt(req,res){

  //console.log(url);  

  mongo.connect(url, function (err, db){

      if (err) {
        console.error(err)
        res.jsonp({error: 'Error de conexión a la base de datos MongoDB.'})
        return;
      }                   
      
      var Falias = req.query.alias;
      var Fnombre = req.query.nombre;
      var Ftxt = req.query.txt.replace("$","#");
      var Fcategoria = req.query.categoria;

      var collection = db.db("twits").collection("posts");      
      collection.insertOne({alias:Falias,nombre:Fnombre, txt : Ftxt, categoria: Fcategoria}, (err, result) => {
              console.log("");
      })
      req.query.txt=req.query.txt.replace("$","#");      
      io.emit('message', req.query);
      res.jsonp({estado : "Exitoso"});      
      
  });  
}

api.post('/twittear',guardarTwitt);

api.get('/limpiar',async function(req,res){
  try{

    await mongo.connect(url, function (err, client){
  
      if (err) {
        console.error(err);      
        return;
      }
    
  
      var base = client.db("twits");
      base.collection("posts").remove({});
      
    });  

  }catch(e){

  }
}
);
app.post('/usuario', async function(req, res) {  

  try{

    console.log(req.body);

    var username = req.body.usuario;
    var name = "";
    var cantidad = 0;
    var arreglo = [];
  
    console.log(username);
  
    await mongo.connect(url, function (err, client){
  
      if (err) {
        console.error(err);      
        return;
      }
    
  
      var base = client.db("twits");
      //base.collection("posts").remove({});
      
      var myPromise = () => {
        return new Promise((resolve, reject) => {     

           base.collection("posts").find({alias : username}).toArray(             
            function(err, data) {
            err 
               ? reject(err) 
               : resolve(data);
          });
        });
      };  

      //Step 2: async promise handler

      var callMyPromise = async () => {
          
        var result = await (myPromise());

        //anything here is executed after result is resolved
        return result;
     };

      //Step 3: make the call

    callMyPromise().then(function(result) {
        client.close();        
        cantidad = result.length;
        console.log("cantidad es "+cantidad);
        name = result[0].nombre;

        if(cantidad < 5){
          arreglo = result;
        }else{
          for(var i = 0; i < 5; i++){
            arreglo.push(result[i])
          }
        }

        res.render('usuario', { username : username, name : name , cantidad : cantidad, twits : result });
     });

    });  

  }catch(e){

  }

});


app.post('/categoria', async function(req, res) {  

  try{

    console.log(req.body);

    var username = req.body.categoria;
    var arreglo = [];
    var cantidad = 0;
  
    console.log(username);
  
    await mongo.connect(url, function (err, client){
  
      if (err) {
        console.error(err);      
        return;
      }
    
  
      var base = client.db("twits");
  
      var myPromise = () => {
        return new Promise((resolve, reject) => {     

           base.collection("posts").find({categoria : username}).toArray(             
            function(err, data) {
            err 
               ? reject(err) 
               : resolve(data);
          });
        });
      };  

      //Step 2: async promise handler

      var callMyPromise = async () => {
          
        var result = await (myPromise());

        //anything here is executed after result is resolved
        return result;
     };

      //Step 3: make the call

    callMyPromise().then(function(result) {
        client.close();        
        cantidad = result.length;
        console.log("cantidad es "+cantidad);
       
        if(cantidad < 3){
          arreglo = result;
        }else{
          for(var i = 0; i < 3; i++){
            arreglo.push(result[i])
          }
        }

        res.render('categoria', {categoria : username, cantidad : cantidad, twits : result });
     });
  
    });  

  }catch(e){

  }
});

//module.exports = { io }; 

server.listen(8000, () => {
  console.log('server is running on port', server.address().port);
});

